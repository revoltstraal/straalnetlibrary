# Straal Payment Gate SDK
This .NET Core Library is to use during integration of Straal Payment to your application. This library is sufficient to integrate Card payment methods using **CryptKey** approach as described in [Straal documentation](https://api-reference.straal.com/#resources-cards-create-a-card-using-cryptkey). Library also implements methods to create transactions using created cards, pre-authorized transactions, captures, refunds and voids. Library **does not** implement other Straal API modules.

## Dependencies
*   [NETStandard.Library 2.0.3](https://www.nuget.org/packages/NETStandard.Library/2.0.3)
*   [Newtonsoft.Json 12.0.1](https://www.nuget.org/packages/Newtonsoft.Json/12.0.1)
*   [RestSharp 106.6.9](https://www.nuget.org/packages/RestSharp/106.6.9)

## Usage
All library functionality is enclosed inside of `StraalConnector` class. Before use there should be instance of this class created. `APIVersion` is optional parameter, default value is `v1`.
```csharp
var connector = new StraalConnector(<APIUrl>, <APIKey>[, <APIVersion>]);
```
Or you can use Dependency Injection like in this example (add to `StartUp`):
```csharp
services.AddScoped(s => new StraalConnector(<APIUrl>, <APIKey>[, <APIVersion>]));
```
## Functionality
### Available methods 
|Method|API call|
| :------------ |:--------------|
|[GetCustomer](#markdown-header-getcustomer) | https://api-reference.straal.com/#resources-customers-get-a-customer|
|[GetCustomers](#markdown-header-getcustomers) | https://api-reference.straal.com/#resources-customers-get-the-customer-list|
|[CreateCustomer](#markdown-header-createcustomer) | https://api-reference.straal.com/#resources-customers-create-a-customer|
|[CreateCryptKey](#markdown-header-createcryptkey) | https://api-reference.straal.com/#resources-cryptkeys-create-a-cryptkey|
|[GetCard](#markdown-header-getcard) | https://api-reference.straal.com/#resources-cards-get-a-card|
|[GetCustomerCards](#markdown-header-getcustomercards) | https://api-reference.straal.com/#resources-cards-get-customers-cards|
|[GetCards](#markdown-header-getcards) | https://api-reference.straal.com/#resources-cards-get-the-card-list|
|[GetTransaction](#markdown-header-gettransaction) | https://api-reference.straal.com/#resources-transactions-get-a-transaction|
|[GetCustomerTransactions](#markdown-header-getcustomertransactions) | https://api-reference.straal.com/#resources-transactions-get-a-list-of-customer-transactions|
|[GetTransactions](#markdown-header-gettransactions) | https://api-reference.straal.com/#resources-transactions-get-a-list-of-transactions|
|[GetCardTransactions](#markdown-header-getcardtransactions) | https://api-reference.straal.com/#resources-transactions-get-transactions-for-a-given-card|
|[CreateCardTransaction](#markdown-header-createcardtransaction) | https://api-reference.straal.com/#resources-transactions-create-a-transaction|
|[CreateFullCapture](#markdown-header-createfullcapture) | https://api-reference.straal.com/#resources-captures-create-a-full-capture|
|[CreatePartialCapture](#markdown-header-createpartialcapture) | https://api-reference.straal.com/#resources-captures-create-a-partial-capture|
|[CreateFullRefund](#markdown-header-createfullrefund) | https://api-reference.straal.com/#resources-refunds-create-a-full-refund|
|[CreatePartialRefund](#markdown-header-createpartialrefund) | https://api-reference.straal.com/#resources-refunds-create-a-partial-refund|
|[CreateVoid](#markdown-header-createvoid) | https://api-reference.straal.com/#resources-voids-create-a-void|

#### Customer
##### GetCustomer
This method simply gets `Customer` info from Straal by its Straal id.
```csharp
var customer = await connector.GetCustomer(<StraalId>);
```
##### GetCustomers
This method returns [`PagedObject<Customer>`](#markdown-header-pagedobjectt) with paged result of getting all customers. Inputs are two optional parameters items per page (`per_page`) and requested page number (`page`).
```csharp
var customers = await connector.GetCustomers([<per_page>, <page>]);
```
##### CreateCustomer
To create a new customer providing email is enough. You can also use optional property `Reference` to save also your unique internal id of a customer on a Straal side.
```csharp
var customer = await connector.CreateCustomer(new Customer { Email = <email>[, Reference = <internalid>] });
```

#### CryptKey
##### CreateCryptKey
Usage of a `CryptKey` is explained in detail in this [Straal documentation page](https://api-reference.straal.com/#resources-cryptkeys-usage). This method creates `CryptKey` for specified permission, Customer Straal id and optionally Transaction data. 
Permissions are:

1. `v1.cards.create` to allow your Front End create a new card
2. `v1.transactions.create_with_card` to allow Front End create a new transaction with a card

```csharp
var key = await connector.CreateCryptKey(new CryptKeyRequest { 
											Permission = <permission>, 
											CustomerId = <customerid> 
											[, Transaction = new TransactionRequest{
													Amount = <amount>,
													Currency = <currency>
													[, Reference = <internalid>]
											}] 
										});
```

#### Card
##### GetCard
This method simply gets `Card` info from Straal by its Straal id.
```csharp
var card = await connector.GetCard(<StraalId>);
```
##### GetCustomerCards
This method returns [`PagedObject<Card>`](#markdown-header-pagedobjectt) with paged result of getting all Cards of a given Customer. Inputs are Customer Straal id and two optional parameters items per page (`per_page`) and requested page number (`page`).
```csharp
var cards = await connector.GetCustomerCards(<CustomerStraalId>[, <per_page>, <page>]);
```
##### GetCards
This method returns [`PagedObject<Card>`](#markdown-header-pagedobjectt) with paged result of getting all Cards of all Customers of a Merchant. Inputs are two optional parameters items per page (`per_page`) and requested page number (`page`).
```csharp
var cards = await connector.GetCards([<per_page>, <page>]);
```

#### Transaction
##### GetTransaction
This method simply gets `Transaction` info from Straal by its Straal id.
```csharp
var transaction = await connector.GetTransaction(<StraalId>);
```
##### GetCustomerTransactions
This method returns [`PagedObject<Transaction>`](#markdown-header-pagedobjectt) with paged result of getting all Transactions of a given Customer. Inputs are Customer Straal id and two optional parameters items per page (`per_page`) and requested page number (`page`).
```csharp
var transactions = await connector.GetCustomerTransactions(<CustomerStraalId>[, <per_page>, <page>]);
```
##### GetTransactions
This method returns [`PagedObject<Transaction>`](#markdown-header-pagedobjectt) with paged result of getting all Transactions of all Customers of a Merchant. Inputs are two optional parameters items per page (`per_page`) and requested page number (`page`).
```csharp
var transactions = await connector.GetTransactions([<per_page>, <page>]);
```
##### GetCardTransactions
This method returns [`PagedObject<Transaction>`](#markdown-header-pagedobjectt) with paged result of getting all Transactions of a given Card. Inputs are Card Straal id and two optional parameters items per page (`per_page`) and requested page number (`page`).
```csharp
var transactions = await connector.GetCardTransactions(<CardStraalId>[, <per_page>, <page>]);
```
##### CreateCardTransaction
Method creates new transaction with a given pre-saved Card. Inputs are Straal Card id and Transaction request object with required `Amount` property (total amount of transaction), `Currency` property, `InitiatedBy` property (by who was transaction initiated, `InitiatedBy.Customer` or `InitiatedBy.Merchant`). Optional property `Capture` indicates whether you wish to immediately capture funds from customers account (if `true`) or to just pre-authorize transaction for this amount and capture funds in the future (if `false`). Optional `Reference` property allows you to save your internal transaction id on Straal side. Default value of `Capture` parameter is `false`.
```csharp
var transaction = await connector.CreateCardTransaction(new TransactionRequest{
																Amount = <amount>,
																Currency = <currency>,
																InitiatedBy = <initiatedby>
																[, Capture = true,
																Reference = <internalid>]
																}, 
														<CardStraalId>);
```

#### Capture
##### CreateFullCapture
Creates a full capture of funds of a given Transaction.
```csharp
var transaction = await connector.CreateFullCapture(<AuthorizedTransactionId>);
```
##### CreatePartialCapture
Creates a partial capture of funds of a given Transaction and specified amount.
```csharp
var transaction = await connector.CreatePartialCapture(<AuthorizedTransactionId>, <amount>);
```

#### Refund
##### CreateFullRefund
Creates a full refund of funds of a given Transaction.
```csharp
var transaction = await connector.CreateFullRefund(<CapturedTransactionId>);
```
##### CreatePartialRefund
Creates a partial refund of funds of a given Transaction and specified amount.
```csharp
var transaction = await connector.CreatePartialRefund(<CapturedTransactionId>, <amount>);
```

#### Void
##### CreateVoid
Creates a void of a given pre-authorized but not yet captured Transaction.
```csharp
var transaction = await connector.CreateVoid(<TransactionId>);
```

### Exception handling
#### StraalException
All SDK methods are throwing `StraalException` in case of Straal API response with not successful state. Thrown exception includes a list of Errors with codes like described in this [Straal Documentation page](https://api-reference.straal.com/#introduction-error-responses).
#### ArgumentException, ArgumentNullException
This exceptions are thrown if input parameters are not set correctly.

### PagedObject<T>
`PagedObject<T>` is returned in all bulk get requests. It has `List` of `T` type with data inside, `Page` property with current page number, `PerPage` propetry with count of entries on one page and `TotalCount` propetry with number of entries on all pages.
```csharp
class PagedObject<T>
{
	List<T> Data;
    int Page;
    int PerPage;
    int TotalCount;
}
```