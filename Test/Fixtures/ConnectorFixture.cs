﻿using StraalSDK;
using StraalSDK.Enums;
using StraalSDK.Models;
using System.Threading.Tasks;

namespace Test.Fixtures
{
    public class ConnectorFixture
    {
        public StraalConnector connector { get; private set; }

        public ConnectorFixture()
        {
            connector = new StraalConnector(Settings.APIUrl, Settings.APIKey);
        }

        public async Task<Transaction> CreatePreAuthorizedTransaction(int amount)
        {
            return await connector.CreateCardTransaction(new TransactionRequest
            {
                Amount = amount,
                Currency = "czk",
                Capture = false,
                InitiatedBy = InitiatedBy.Merchant
            }, Settings.ValidCardStraalId);
        }
        
        public async Task<Transaction> CreateTransaction(int amount)
        {
            return await connector.CreateCardTransaction(new TransactionRequest
            {
                Amount = amount,
                Currency = "czk",
                Capture = true,
                InitiatedBy = InitiatedBy.Merchant
            }, Settings.ValidCardStraalId);
        }
    }
}
