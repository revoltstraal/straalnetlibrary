using StraalSDK;
using StraalSDK.Enums;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class RefundTest
    {
        private readonly ConnectorFixture _fixture;

        public RefundTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidFullRefund()
        {
            var transaction = await _fixture.CreateTransaction(100);

            transaction = await _fixture.connector.CreateFullRefund(transaction.Id);
            Assert.NotNull(transaction);
            Assert.True(transaction.Refunded);
        }

        [Fact]
        public async Task ValidPartialRefund()
        {
            var transaction = await _fixture.CreateTransaction(100);

            transaction = await _fixture.connector.CreatePartialRefund(transaction.Id, 50);
            Assert.NotNull(transaction);
            Assert.NotEmpty(transaction.Refunds);
        }

        [Fact]
        public async Task NotFoundPartialCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreatePartialRefund(Settings.NotFoundTransactionStraalId, 50); });
            Assert.Equal(40402, exception.Errors.First().Code);//Transaction not found
        }

        [Fact]
        public async Task NotFoundFullCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateFullRefund(Settings.NotFoundTransactionStraalId); });
            Assert.Equal(40402, exception.Errors.First().Code);//Transaction not found
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task BadAmountPartialCreate(int amount)
        {
            var transaction = await _fixture.CreateTransaction(100);

            await Assert.ThrowsAsync<ArgumentException>(async delegate { await _fixture.connector.CreatePartialRefund(transaction.Id, amount); });
        }

        [Theory]
        [InlineData(100)]
        public async Task TooHighAmountPartialCreate(int amount)
        {
            var transaction = await _fixture.CreateTransaction(amount);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreatePartialRefund(transaction.Id, amount + 1); });
            Assert.Equal(50007, exception.Errors.First().Code);//Amount is higher than captured
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task NullFullCreate(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.CreateFullRefund(id); });
        }

        [Theory]
        [InlineData(null, 100)]
        [InlineData("", 100)]
        public async Task NullPartialCreate(string id, int amount)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.CreatePartialRefund(id, amount); });
        }

        [Fact]
        public async Task NotYetCapturedFullCreate()
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(100);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateFullRefund(transaction.Id); });
            Assert.Equal(50011, exception.Errors.First().Code);//Transaction is not yet captured
        }

        [Fact]
        public async Task NotYetCapturedPartialCreate()
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(100);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreatePartialRefund(transaction.Id, 50); });
            Assert.Equal(50011, exception.Errors.First().Code);//Transaction is not yet captured
        }
    }
}
