using StraalSDK;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class CustomerTest
    {
        private readonly ConnectorFixture _fixture;

        public CustomerTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidCreate()
        {
            var customer = await _fixture.connector.CreateCustomer(new Customer { Email = Settings.ValidCustomerEmail });
            Assert.NotNull(customer);
        }

        [Fact]
        public async Task CreateBadEmail()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateCustomer(new Customer { Email = Settings.InvalidCustomerEmail }); });
            Assert.Equal(12002, exception.Errors.First().Code);//Invalid email
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task CreateEmailNull(string email)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.CreateCustomer(new Customer { Email = email }); });
        }

        [Fact]
        public async Task ValidGet()
        {
            var customer = await _fixture.connector.GetCustomer(Settings.ValidCustomerStraalId);
            Assert.NotNull(customer);
        }

        [Fact]
        public async Task NotFoundGet()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomer(Settings.NotFoundCustomerStraalId); });
            Assert.Equal(40403, exception.Errors.First().Code);//Customer not found
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NullGet(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.GetCustomer(id); });
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        public async Task ValidGetList(int perpage)
        {
            var customers = await _fixture.connector.GetCustomers(perpage);
            Assert.NotNull(customers);
            while(customers.Page * customers.PerPage < customers.TotalCount)
            {
                customers = await _fixture.connector.GetCustomers(perpage, customers.Page + 1);
                Assert.NotNull(customers);
            }
        }

        [Fact]
        public async Task TooHighPerPageLimitGetList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomers(Settings.PerPageTooHighLimit); });
            Assert.Equal(94002, exception.Errors.First().Code);//Too high limit per page, max 100
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task NegativePerPageLimitGetList(int perpage)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomers(perpage); });
            Assert.Equal(94001, exception.Errors.First().Code);//Invalid param per_page
        }

        [Fact]
        public async Task PageOutOfBoundGetList()
        {
            var customers = await _fixture.connector.GetCustomers(10);
            while (customers.Page * customers.PerPage < customers.TotalCount)
            {
                customers = await _fixture.connector.GetCustomers(customers.PerPage, customers.Page + 1);
            }
            customers =  await _fixture.connector.GetCustomers(customers.PerPage, customers.Page + 1);
            Assert.Empty(customers.Data);
        }
    }
}
