using StraalSDK;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class CardTest
    {
        private readonly ConnectorFixture _fixture;

        public CardTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidGet()
        {
            var card = await _fixture.connector.GetCard(Settings.ValidCardStraalId);
            Assert.NotNull(card);
        }

        [Fact]
        public async Task NotFoundGet()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCard(Settings.NotFoundCardStraalId); });
            Assert.Equal(40404, exception.Errors.First().Code);//Card not found
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NullGet(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.GetCard(id); });
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        public async Task ValidGetList(int perpage)
        {
            var cards = await _fixture.connector.GetCards(perpage);
            Assert.NotNull(cards);
            while(cards.Page * cards.PerPage < cards.TotalCount)
            {
                cards = await _fixture.connector.GetCards(perpage, cards.Page + 1);
                Assert.NotNull(cards);
            }
        }

        [Fact]
        public async Task TooHighPerPageLimitGetList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCards(Settings.PerPageTooHighLimit); });
            Assert.Equal(94002, exception.Errors.First().Code);//Too high limit per page, max 100
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task NegativePerPageLimitGetList(int perpage)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCards(perpage); });
            Assert.Equal(94001, exception.Errors.First().Code);//Invalid param per_page
        }

        [Fact]
        public async Task PageOutOfBoundGetList()
        {
            var cards = await _fixture.connector.GetCards(10);
            while (cards.Page * cards.PerPage < cards.TotalCount)
            {
                cards = await _fixture.connector.GetCards(cards.PerPage, cards.Page + 1);
            }
            cards =  await _fixture.connector.GetCards(cards.PerPage, cards.Page + 1);
            Assert.Empty(cards.Data);
        }




        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NullGetCustomerCardsList(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.GetCustomerCards(id); });
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        public async Task ValidGetCustomerCardsList(int perpage)
        {
            var cards = await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, perpage);
            Assert.NotNull(cards);
            while (cards.Page * cards.PerPage < cards.TotalCount)
            {
                cards = await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, perpage, cards.Page + 1);
                Assert.NotNull(cards);
            }
        }

        [Fact]
        public async Task TooHighPerPageLimitGetCustomerCardsList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, Settings.PerPageTooHighLimit); });
            Assert.Equal(94002, exception.Errors.First().Code);//Too high limit per page, max 100
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task NegativePerPageLimitGetCustomerCardsList(int perpage)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, perpage); });
            Assert.Equal(94001, exception.Errors.First().Code);//Invalid param per_page
        }

        [Fact]
        public async Task PageOutOfBoundGetCustomerCardsList()
        {
            var cards = await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, 10);
            while (cards.Page * cards.PerPage < cards.TotalCount)
            {
                cards = await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, cards.PerPage, cards.Page + 1);
            }
            cards = await _fixture.connector.GetCustomerCards(Settings.ValidCustomerStraalId, cards.PerPage, cards.Page + 1);
            Assert.Empty(cards.Data);
        }

        [Fact]
        public async Task NotFoundCustomerGetCustomerCardsList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomerCards(Settings.NotFoundCustomerStraalId); });
            Assert.Equal(40403, exception.Errors.First().Code);//Customer not found
        }
    }
}
