using StraalSDK;
using StraalSDK.Enums;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class TransactionTest
    {
        private readonly ConnectorFixture _fixture;

        public TransactionTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidGet()
        {
            var transaction = await _fixture.connector.GetTransaction(Settings.ValidTransactionStraalId);
            Assert.NotNull(transaction);
        }

        [Fact]
        public async Task NotFoundGet()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetTransaction(Settings.NotFoundTransactionStraalId); });
            Assert.Equal(40402, exception.Errors.First().Code);//Transaction not found
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NullGet(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.GetTransaction(id); });
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        public async Task ValidGetList(int perpage)
        {
            var transactions = await _fixture.connector.GetTransactions(perpage);
            Assert.NotNull(transactions);
            while(transactions.Page * transactions.PerPage < transactions.TotalCount)
            {
                transactions = await _fixture.connector.GetTransactions(perpage, transactions.Page + 1);
                Assert.NotNull(transactions);
            }
        }

        [Fact]
        public async Task TooHighPerPageLimitGetList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetTransactions(Settings.PerPageTooHighLimit); });
            Assert.Equal(94002, exception.Errors.First().Code);//Too high limit per page, max 100
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task NegativePerPageLimitGetList(int perpage)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetTransactions(perpage); });
            Assert.Equal(94001, exception.Errors.First().Code);//Invalid param per_page
        }

        [Fact]
        public async Task PageOutOfBoundGetList()
        {
            var transactions = await _fixture.connector.GetTransactions(10);
            while (transactions.Page * transactions.PerPage < transactions.TotalCount)
            {
                transactions = await _fixture.connector.GetTransactions(transactions.PerPage, transactions.Page + 1);
            }
            transactions =  await _fixture.connector.GetTransactions(transactions.PerPage, transactions.Page + 1);
            Assert.Empty(transactions.Data);
        }



        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NullGetCustomerTransactionsList(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.GetCustomerTransactions(id); });
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        public async Task ValidGetCustomerTransactionsList(int perpage)
        {
            var transactions = await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, perpage);
            Assert.NotNull(transactions);
            while (transactions.Page * transactions.PerPage < transactions.TotalCount)
            {
                transactions = await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, perpage, transactions.Page + 1);
                Assert.NotNull(transactions);
            }
        }

        [Fact]
        public async Task TooHighPerPageLimitGetCustomerTransactionsList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, Settings.PerPageTooHighLimit); });
            Assert.Equal(94002, exception.Errors.First().Code);//Too high limit per page, max 100
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task NegativePerPageLimitGetCustomerTransactionsList(int perpage)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, perpage); });
            Assert.Equal(94001, exception.Errors.First().Code);//Invalid param per_page
        }

        [Fact]
        public async Task PageOutOfBoundGetCustomerTransactionsList()
        {
            var transactions = await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, 10);
            while (transactions.Page * transactions.PerPage < transactions.TotalCount)
            {
                transactions = await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, transactions.PerPage, transactions.Page + 1);
            }
            transactions = await _fixture.connector.GetCustomerTransactions(Settings.ValidCustomerStraalId, transactions.PerPage, transactions.Page + 1);
            Assert.Empty(transactions.Data);
        }

        [Fact]
        public async Task NotFoundCustomerGetCustomerTransactionsList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCustomerTransactions(Settings.NotFoundCustomerStraalId); });
            Assert.Equal(40403, exception.Errors.First().Code);//Customer not found
        }



        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NullGetCardTransactionsList(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.GetCardTransactions(id); });
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        public async Task ValidGetCardTransactionsList(int perpage)
        {
            var transactions = await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, perpage);
            Assert.NotNull(transactions);
            while (transactions.Page * transactions.PerPage < transactions.TotalCount)
            {
                transactions = await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, perpage, transactions.Page + 1);
                Assert.NotNull(transactions);
            }
        }

        [Fact]
        public async Task TooHighPerPageLimitGetCardTransactionsList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, Settings.PerPageTooHighLimit); });
            Assert.Equal(94002, exception.Errors.First().Code);//Too high limit per page, max 100
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task NegativePerPageLimitGetCardTransactionsList(int perpage)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, perpage); });
            Assert.Equal(94001, exception.Errors.First().Code);//Invalid param per_page
        }

        [Fact]
        public async Task PageOutOfBoundGetCardTransactionsList()
        {
            var transactions = await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, 10);
            while (transactions.Page * transactions.PerPage < transactions.TotalCount)
            {
                transactions = await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, transactions.PerPage, transactions.Page + 1);
            }
            transactions = await _fixture.connector.GetCardTransactions(Settings.ValidCardStraalId, transactions.PerPage, transactions.Page + 1);
            Assert.Empty(transactions.Data);
        }

        [Fact]
        public async Task NotFoundCardGetCardTransactionsList()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.GetCardTransactions(Settings.NotFoundCardStraalId); });
            Assert.Equal(40404, exception.Errors.First().Code);//Card not found
        }

        [Theory]
        [InlineData(100, "czk", InitiatedBy.Merchant)]
        [InlineData(50, "eur", InitiatedBy.Customer)]
        public async Task ValidCreate(int amount, string currency, InitiatedBy initiatedBy)
        {
            var transaction = await _fixture.connector.CreateCardTransaction(new TransactionRequest
            {
                Amount = amount,
                Currency = currency,
                Capture = true,
                InitiatedBy = initiatedBy
            }, Settings.ValidCardStraalId);
            Assert.NotNull(transaction);
            Assert.True(transaction.Captured);
        }

        [Theory]
        [InlineData(100, "czk", InitiatedBy.Merchant)]
        [InlineData(50, "eur", InitiatedBy.Customer)]
        public async Task ValidPreAuthorizedCreate(int amount, string currency, InitiatedBy initiatedBy)
        {
            var transaction = await _fixture.connector.CreateCardTransaction(new TransactionRequest
            {
                Amount = amount,
                Currency = currency,
                Capture = false,
                InitiatedBy = initiatedBy
            }, Settings.ValidCardStraalId);
            Assert.NotNull(transaction);
            Assert.False(transaction.Captured);
        }

        [Theory]
        [InlineData(100, "czk", InitiatedBy.Merchant)]
        [InlineData(50, "eur", InitiatedBy.Customer)]
        public async Task BadCardIdCreate(int amount, string currency, InitiatedBy initiatedBy)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate {
                await _fixture.connector.CreateCardTransaction(new TransactionRequest
                {
                    Amount = amount,
                    Currency = currency,
                    InitiatedBy = initiatedBy
                }, Settings.NotFoundCardStraalId);
            });

            Assert.Equal(40404, exception.Errors.First().Code);//Card not found
        }

        [Theory]
        [InlineData(0, "czk", InitiatedBy.Merchant)]
        [InlineData(-1, "eur", InitiatedBy.Customer)]
        public async Task BadAmountTransactionCreate(int amount, string currency, InitiatedBy initiatedBy)
        {
            await Assert.ThrowsAsync<ArgumentException>(async delegate {
                await _fixture.connector.CreateCardTransaction(new TransactionRequest
                {
                    Amount = amount,
                    Currency = currency,
                    InitiatedBy = initiatedBy
                }, Settings.ValidCardStraalId);
            });
        }

        [Theory]
        [InlineData(100, "aaa", InitiatedBy.Merchant)]
        public async Task BadCurrencyTransactionCreate(int amount, string currency, InitiatedBy initiatedBy)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate {
                await _fixture.connector.CreateCardTransaction(new TransactionRequest
                {
                    Amount = amount,
                    Currency = currency,
                    InitiatedBy = initiatedBy
                }, Settings.ValidCardStraalId);
            });
            Assert.Equal(10262, exception.Errors.First().Code);//Invalid currency
        }


        [Theory]
        [InlineData(100, null, InitiatedBy.Merchant)]
        [InlineData(100, "", InitiatedBy.Customer)]
        public async Task MissingCurrencyTransactionCreate(int amount, string currency, InitiatedBy initiatedBy)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate {
                await _fixture.connector.CreateCardTransaction(new TransactionRequest
                {
                    Amount = amount,
                    Currency = currency,
                    InitiatedBy = initiatedBy
                }, Settings.ValidCardStraalId);
            });
        }
    }
}
