using StraalSDK;
using StraalSDK.Enums;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class VoidTest
    {
        private readonly ConnectorFixture _fixture;

        public VoidTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidCreate()
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(100);

            transaction = await _fixture.connector.CreateVoid(transaction.Id);
            Assert.NotNull(transaction);
            Assert.True(transaction.Voided);
        }

        [Fact]
        public async Task NotFoundCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateVoid(Settings.NotFoundTransactionStraalId); });
            Assert.Equal(40402, exception.Errors.First().Code);//Transaction not found
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task NullCreate(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.CreateVoid(id); });
        }

        [Fact]
        public async Task AlreadyCapturedCreate()
        {
            var transaction = await _fixture.CreateTransaction(100);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateVoid(transaction.Id); });
            Assert.Equal(25005, exception.Errors.First().Code);//Transaction has been already captured
        }
    }
}
