using StraalSDK;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class CryptKeyTest
    {
        private readonly ConnectorFixture _fixture;

        public CryptKeyTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidCardCreate()
        {
            var key = await _fixture.connector.CreateCryptKey(new CryptKeyRequest {
                CustomerId = Settings.ValidCustomerStraalId,
                Permission = Settings.CardCreatePermission });

            Assert.NotNull(key);
        }

        [Fact]
        public async Task BadCustomerIdCardCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateCryptKey(new CryptKeyRequest {
                CustomerId = Settings.NotFoundCustomerStraalId,
                Permission = Settings.CardCreatePermission }); });

            Assert.Equal(10817, exception.Errors.First().Code);//Invalid customer_id - not found
        }

        [Fact]
        public async Task BadPermissionCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate {
                await _fixture.connector.CreateCryptKey(new CryptKeyRequest
                {
                    CustomerId = Settings.ValidCustomerStraalId,
                    Permission = Settings.BadPermission
                });
            });

            Assert.Equal(10801, exception.Errors.First().Code);//Bad CryptKey permission
        }

        [Theory]
        [InlineData(100, "czk")]
        [InlineData(20, "eur")]
        public async Task ValidTransactionCreate(int amount, string currency)
        {
            var key = await _fixture.connector.CreateCryptKey(new CryptKeyRequest {
                CustomerId = Settings.ValidCustomerStraalId,
                Permission = Settings.TransactionCreatePermission,
                Transaction = new TransactionCryptKeyRequest
                {
                    Amount = amount,
                    Currency = currency
                }
            });
            Assert.NotNull(key);
        }

        [Theory]
        [InlineData(0, "czk")]
        [InlineData(-1, "eur")]
        public async Task BadAmountTransactionCreate(int amount, string currency)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate {
                await _fixture.connector.CreateCryptKey(new CryptKeyRequest
                {
                    CustomerId = Settings.ValidCustomerStraalId,
                    Permission = Settings.TransactionCreatePermission,
                    Transaction = new TransactionCryptKeyRequest
                    {
                        Amount = amount,
                        Currency = currency
                    }
                });
            });
            Assert.Equal(10263, exception.Errors.First().Code);//Invalid amount value
        }

        [Theory]
        [InlineData(100, "aaa")]
        [InlineData(100, "")]
        public async Task BadCurrencyTransactionCreate(int amount, string currency)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate {
                await _fixture.connector.CreateCryptKey(new CryptKeyRequest
                {
                    CustomerId = Settings.ValidCustomerStraalId,
                    Permission = Settings.TransactionCreatePermission,
                    Transaction = new TransactionCryptKeyRequest
                    {
                        Amount = amount,
                        Currency = currency
                    }
                });
            });
            Assert.Equal(10262, exception.Errors.First().Code);//Invalid currency
        }


        [Theory]
        [InlineData(100, null)]
        public async Task MissingCurrencyTransactionCreate(int amount, string currency)
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate {
                await _fixture.connector.CreateCryptKey(new CryptKeyRequest
                {
                    CustomerId = Settings.ValidCustomerStraalId,
                    Permission = Settings.TransactionCreatePermission,
                    Transaction = new TransactionCryptKeyRequest
                    {
                        Amount = amount,
                        Currency = currency
                    }
                });
            });
            Assert.Equal(10265, exception.Errors.First().Code);//Missing currency
        }
    }
}
