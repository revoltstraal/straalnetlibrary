using StraalSDK;
using StraalSDK.Enums;
using StraalSDK.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Test.Fixtures;
using Xunit;

namespace Test
{
    [Collection("ConnectorCollection")]
    public class CaptureTest
    {
        private readonly ConnectorFixture _fixture;

        public CaptureTest(ConnectorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task ValidFullCreate()
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(100);

            transaction = await _fixture.connector.CreateFullCapture(transaction.Id);
            Assert.NotNull(transaction);
            Assert.True(transaction.Captured);
        }

        [Fact]
        public async Task ValidPartialCreate()
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(100);

            transaction = await _fixture.connector.CreatePartialCapture(transaction.Id, 50);
            Assert.NotNull(transaction);
            Assert.True(transaction.Captured);
        }

        [Fact]
        public async Task NotFoundPartialCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreatePartialCapture(Settings.NotFoundTransactionStraalId, 50); });
            Assert.Equal(40402, exception.Errors.First().Code);//Transaction not found
        }

        [Fact]
        public async Task NotFoundFullCreate()
        {
            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateFullCapture(Settings.NotFoundTransactionStraalId); });
            Assert.Equal(40402, exception.Errors.First().Code);//Transaction not found
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task BadAmountPartialCreate(int amount)
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(100);

            await Assert.ThrowsAsync<ArgumentException>(async delegate { await _fixture.connector.CreatePartialCapture(transaction.Id, amount); });
        }

        [Theory]
        [InlineData(100)]
        public async Task TooHighAmountPartialCreate(int amount)
        {
            var transaction = await _fixture.CreatePreAuthorizedTransaction(amount);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreatePartialCapture(transaction.Id, amount + 1); });
            Assert.Equal(24001, exception.Errors.First().Code);//Amount is higher than authorized
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task NullFullCreate(string id)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.CreateFullCapture(id); });
        }

        [Theory]
        [InlineData(null, 100)]
        [InlineData("", 100)]
        public async Task NullPartialCreate(string id, int amount)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async delegate { await _fixture.connector.CreatePartialCapture(id, amount); });
        }

        [Fact]
        public async Task AlreadyCapturedFullCreate()
        {
            var transaction = await _fixture.CreateTransaction(100);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreateFullCapture(transaction.Id); });
            Assert.Equal(24003, exception.Errors.First().Code);//Transaction has been already captured
        }

        [Fact]
        public async Task AlreadyCapturedPartialCreate()
        {
            var transaction = await _fixture.CreateTransaction(100);

            var exception = await Assert.ThrowsAsync<StraalException>(async delegate { await _fixture.connector.CreatePartialCapture(transaction.Id, 50); });
            Assert.Equal(24003, exception.Errors.First().Code);//Transaction has been already captured
        }
    }
}
