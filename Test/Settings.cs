﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public static class Settings
    {
        public static string APIKey = "";//Put your Straal Test API Key here.
        public static string APIUrl = @"https://api.straal.com";

        public static string ValidCustomerEmail = "test@email.com";
        public static string InvalidCustomerEmail = "bademail";
        public static string ValidCustomerStraalId = "";//Put valid customer id here.
        public static string NotFoundCustomerStraalId = "1234567891111";
        public static string ValidCardStraalId = "";//Put valid card id here.
        public static string NotFoundCardStraalId = "1234567891111";
        public static string ValidTransactionStraalId = "";//Put valid transaction id here.
        public static string NotFoundTransactionStraalId = "1234567891111";

        public static int PerPageTooHighLimit = 101;

        public static string CardCreatePermission = "v1.cards.create";
        public static string TransactionCreatePermission = "v1.transactions.create_with_card";
        public static string BadPermission = "v1.bad.permission";
    }
}
