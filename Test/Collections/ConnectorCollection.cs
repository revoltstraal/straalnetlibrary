﻿using System;
using System.Collections.Generic;
using System.Text;
using Test.Fixtures;
using Xunit;

namespace Test.Collections
{
    [CollectionDefinition("ConnectorCollection")]
    public class ConnectorCollection : ICollectionFixture<ConnectorFixture>
    {
    }
}
