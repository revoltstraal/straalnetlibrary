﻿using Newtonsoft.Json;
using StraalSDK.Enums;
using System;

namespace StraalSDK.Converters
{
    internal sealed class InitiatedByConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
                object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "customer":
                    return InitiatedBy.Customer;
                case "merchant":
                    return InitiatedBy.Merchant;
                default:
                    return InitiatedBy.Unknown;
            }
        }

        public override void WriteJson(JsonWriter writer, object value,
                JsonSerializer serializer)
        {
            var EnumVal = (InitiatedBy)value;

            switch (EnumVal)
            {
                case InitiatedBy.Customer:
                    writer.WriteValue("customer");
                    break;
                case InitiatedBy.Merchant:
                    writer.WriteValue("merchant");
                    break;
                default:
                    return;
            }
        }
    }
}
