﻿using Newtonsoft.Json;
using StraalSDK.Enums;
using System;

namespace StraalSDK.Converters
{
    internal sealed class GeneralStatusConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
                object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "succeeded":
                    return GeneralStatus.Succeed;
                case "pending":
                    return GeneralStatus.Pending;
                case "failed":
                    return GeneralStatus.Failed;
                default:
                    return GeneralStatus.Unknown;
            }
        }

        public override void WriteJson(JsonWriter writer, object value,
                JsonSerializer serializer)
        {
            var EnumVal = (GeneralStatus)value;

            switch (EnumVal)
            {
                case GeneralStatus.Succeed:
                    writer.WriteValue("succeeded");
                    break;
                case GeneralStatus.Pending:
                    writer.WriteValue("pending");
                    break;
                case GeneralStatus.Failed:
                    writer.WriteValue("failed");
                    break;
                default:
                    return;
            }
        }
    }
}
