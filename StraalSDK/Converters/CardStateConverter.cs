﻿using Newtonsoft.Json;
using StraalSDK.Enums;
using System;

namespace StraalSDK.Converters
{
    internal sealed class CardStateConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
                object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "active":
                    return CardState.Active;
                case "disabled":
                    return CardState.Disabled;
                default:
                    return CardState.Unknown;
            }
        }

        public override void WriteJson(JsonWriter writer, object value,
                JsonSerializer serializer)
        {
            var EnumVal = (CardState)value;

            switch (EnumVal)
            {
                case CardState.Active:
                    writer.WriteValue("active");
                    break;
                case CardState.Disabled:
                    writer.WriteValue("disabled");
                    break;
                default:
                    return;
            }
        }
    }
}
