﻿using Newtonsoft.Json;
using StraalSDK.Enums;
using System;

namespace StraalSDK.Converters
{
    internal sealed class PaymentMethodConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
                object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "card":
                    return PaymentMethod.Card;
                case "sepa":
                    return PaymentMethod.SEPA;
                case "pay_by_link":
                    return PaymentMethod.PayByLink;
                default:
                    return PaymentMethod.Unknown;
            }
        }

        public override void WriteJson(JsonWriter writer, object value,
                JsonSerializer serializer)
        {
            var EnumVal = (PaymentMethod)value;

            switch (EnumVal)
            {
                case PaymentMethod.Card:
                    writer.WriteValue("card");
                    break;
                case PaymentMethod.SEPA:
                    writer.WriteValue("sepa");
                    break;
                case PaymentMethod.PayByLink:
                    writer.WriteValue("pay_by_link");
                    break;
                default:
                    return;
            }
        }
    }
}
