﻿using StraalSDK.Models;
using System;
using System.Collections.Generic;

namespace StraalSDK
{
    /// <summary>
    /// Exception throwned if request to server was unsuccessful.
    /// </summary>
    public class StraalException : Exception
    {
        /// <summary>
        /// List of errors with codes and desriptions.
        /// </summary>
        public List<Error> Errors { get; private set; } = new List<Error>();

        internal StraalException(ErrorResponse response)
            : base(response.Message)
        {
            Errors = response.Errors;
        }

        internal StraalException(string message)
            : base(message)
        {
            Errors.Add(new Error { Message = message });
        }

        internal StraalException(string message, Exception inner)
            : base(message, inner)
        {
            Errors.Add(new Error { Message = message });
        }
    }
}
