﻿namespace StraalSDK.Enums
{
    /// <summary>
    /// Represents status of captures, refunds and voids.
    /// </summary>
    public enum GeneralStatus
    {
        Succeed,
        Pending,
        Failed,
        Unknown
    }
}
