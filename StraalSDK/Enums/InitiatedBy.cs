﻿namespace StraalSDK.Enums
{
    /// <summary>
    /// Indicates who initiated a transaction.
    /// </summary>
    public enum InitiatedBy
    {
        Customer,
        Merchant,
        Unknown
    }
}
