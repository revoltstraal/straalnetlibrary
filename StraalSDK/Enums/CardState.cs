﻿namespace StraalSDK.Enums
{
    /// <summary>
    /// Represents state of a card.
    /// </summary>
    public enum CardState
    {
        Active,
        Disabled,
        Unknown
    }
}
