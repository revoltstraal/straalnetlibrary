﻿namespace StraalSDK.Enums
{
    /// <summary>
    /// Represents payment type of a transaction.
    /// </summary>
    public enum PaymentMethod
    {
        Card,
        SEPA,
        PayByLink,
        Unknown
    }
}
