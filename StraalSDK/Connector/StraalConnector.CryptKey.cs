﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Creates CryptKey with a specified permission. CryptKey then gives that permission to the front-end to make specified action directly.
        /// </summary>
        /// <param name="request">In a request object at least customer id and permission string must be specified. See library or Straal documentation for further details.</param>
        /// <exception cref="ArgumentNullException">When customer id or permission string is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<CryptKey> CreateCryptKey(CryptKeyRequest request)
        {
            if (string.IsNullOrEmpty(request.CustomerId))
            {
                throw new ArgumentNullException(nameof(request.CustomerId));
            }
            if (string.IsNullOrEmpty(request.Permission))
            {
                throw new ArgumentNullException(nameof(request.Permission));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/cryptkeys", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(request);

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<CryptKey>(response);
        }
    }
}
