﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="customer">Customer object must have email field set. Optionally reference field with customers internal uniq code (id) can be set.</param>
        /// <exception cref="ArgumentNullException">When customer email field is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Customer> CreateCustomer(Customer customer)
        {
            if (string.IsNullOrEmpty(customer.Email))
            {
                throw new ArgumentNullException(nameof(customer.Email));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/customers", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(customer);

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Customer>(response);
        }

        /// <summary>
        /// Returns customer information.
        /// </summary>
        /// <param name="id">Customer id.</param>
        /// <exception cref="ArgumentNullException">When id is null or empty</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Customer> GetCustomer(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/customers/" + id, Method.GET);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Customer>(response);
        }

        /// <summary>
        /// Returns all customers information in a paged object.
        /// </summary>
        /// <param name="perpage">Number of entries per page. Default is 3.</param>
        /// <param name="page">Requested page number. Default is 1.</param>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<PagedObject<Customer>> GetCustomers(int perpage = 3, int page = 1)
        {
            var restRequest = new RestRequest(@"/" + _APIVersion + "/customers", Method.GET);

            restRequest.AddQueryParameter("per_page", perpage.ToString());
            restRequest.AddQueryParameter("page", page.ToString());

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<PagedObject<Customer>>(response);
        }
    }
}
