﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Returns transaction information .
        /// </summary>
        /// <param name="id">Transaction id.</param>
        /// <exception cref="ArgumentNullException">When id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> GetTransaction(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/transactions/" + id, Method.GET);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }

        /// <summary>
        /// Returns customer transactions information in a paged object.
        /// </summary>
        /// <param name="id">Customer id.</param>
        /// <param name="perpage">Number of entries per page. Default is 3.</param>
        /// <param name="page">Requested page number. Default is 1.</param>
        /// <exception cref="ArgumentNullException">When id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<PagedObject<Transaction>> GetCustomerTransactions(string id, int perpage = 3, int page = 1)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/customers/" + id + "/transactions", Method.GET);

            restRequest.AddQueryParameter("per_page", perpage.ToString());
            restRequest.AddQueryParameter("page", page.ToString());

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<PagedObject<Transaction>>(response);
        }

        /// <summary>
        /// Returns all transactions information of all merchant customers in a paged object.
        /// </summary>
        /// <param name="perpage">Number of entries per page. Default is 3.</param>
        /// <param name="page">Requested page number. Default is 1.</param>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<PagedObject<Transaction>> GetTransactions(int perpage = 3, int page = 1)
        {
            var restRequest = new RestRequest(@"/" + _APIVersion + "/transactions", Method.GET);

            restRequest.AddQueryParameter("per_page", perpage.ToString());
            restRequest.AddQueryParameter("page", page.ToString());

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<PagedObject<Transaction>>(response);
        }

        /// <summary>
        /// Returns card transactions information in a paged object.
        /// </summary>
        /// <param name="id">Card id.</param>
        /// <param name="perpage">Number of entries per page. Default is 3.</param>
        /// <param name="page">Requested page number. Default is 1.</param>
        /// <exception cref="ArgumentNullException">When is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<PagedObject<Transaction>> GetCardTransactions(string id, int perpage = 3, int page = 1)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/cards/" + id + "/transactions", Method.GET);

            restRequest.AddQueryParameter("per_page", perpage.ToString());
            restRequest.AddQueryParameter("page", page.ToString());

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<PagedObject<Transaction>>(response);
        }

        /// <summary>
        /// Cretaes new transaction for a given pre-saved card.
        /// </summary>
        /// <param name="request">In a request object there must be specified at least amount you want to capture, currency and by whom is transaction initiated (customer or merchant).
        /// Optionally capture field could be set to true if transaction should be captured immediately. Otherwise pre-authorized transaction will be created.
        /// Optionally reference field could be set indicating internal transaction uniq code (id).
        /// </param>
        /// <param name="cardId">Card id.</param>
        /// <exception cref="ArgumentNullException">When currency or card id is null or empty.</exception>
        /// <exception cref="ArgumentException">When amount is less or equal to zero.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> CreateCardTransaction(TransactionRequest request, string cardId)
        {
            if (string.IsNullOrEmpty(request.Currency))
            {
                throw new ArgumentNullException(nameof(request.Currency));
            }
            if (string.IsNullOrEmpty(cardId))
            {
                throw new ArgumentNullException(nameof(cardId));
            }
            if (request.Amount <= 0)
            {
                throw new ArgumentException("Is less or equal to zero", nameof(request.Amount));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/cards/" + cardId + "/transactions", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(request);

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }
    }
}
