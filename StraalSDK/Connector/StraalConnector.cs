﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Text;

namespace StraalSDK
{
    /// <summary>
    /// Class represents implementation of a connector to the Straal API. Create a new instance to access its functionality.
    /// </summary>
    public partial class StraalConnector
    {
        internal static RestClient Client { get; private set; }
        private readonly string _APIVersion;
        
        /// <summary>
        /// Creates new instance of Straal Connector with Basic auth by your API Key
        /// </summary>
        /// <param name="APIUrl">URL of Straal API</param>
        /// <param name="APIKey">Your Straal API Key</param>
        /// <param name="APIVersion">Version of Straal API. Default is "v1"</param>
        public StraalConnector(string APIUrl, string APIKey, string APIVersion = "v1")
        {
            Client = new RestClient();
            Client.BaseUrl = new Uri(APIUrl);
            Client.UserAgent = "Straal .NET Client";
            var encoding = new ASCIIEncoding();
            Client.AddDefaultHeader("Authorization", "Basic " + Convert.ToBase64String(encoding.GetBytes(string.Format(":{0}", APIKey))));
            _APIVersion = APIVersion;
        }

        private string SerializeToJson(object o)
        {
            return JsonConvert.SerializeObject(o, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

        private T ProcessResponse<T>(IRestResponse response)
        {
            try
            {
                return Deserialize<T>(response.Content);
            }
            catch
            {
                throw new StraalException(response.Content);
            }
        }

        private T Deserialize<T>(string Content)
        {
            return JsonConvert.DeserializeObject<T>(Content);
        }
    }
}
