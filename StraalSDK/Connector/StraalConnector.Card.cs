﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Returns card information.
        /// </summary>
        /// <param name="id">Card id.</param>
        /// <exception cref="ArgumentNullException">When id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Card> GetCard(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/cards/" + id, Method.GET);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Card>(response);
        }

        /// <summary>
        /// Returns customer cards information in a paged object.
        /// </summary>
        /// <param name="id">Customer id.</param>
        /// <param name="perpage">Number of entries per page. Default is 3.</param>
        /// <param name="page">Requested page number. Default is 1.</param>
        /// <exception cref="ArgumentNullException">When id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<PagedObject<Card>> GetCustomerCards(string id, int perpage = 3, int page = 1)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/customers/" + id + "/cards", Method.GET);

            restRequest.AddQueryParameter("per_page", perpage.ToString());
            restRequest.AddQueryParameter("page", page.ToString());

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<PagedObject<Card>>(response);
        }

        /// <summary>
        /// Returns all cards information of all merchant customers in a paged object.
        /// </summary>
        /// <param name="perpage">Number of entries per page. Default is 3.</param>
        /// <param name="page">Requested page number. Default is 1.</param>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<PagedObject<Card>> GetCards(int perpage = 3, int page = 1)
        {
            var restRequest = new RestRequest(@"/" + _APIVersion + "/cards", Method.GET);

            restRequest.AddQueryParameter("per_page", perpage.ToString());
            restRequest.AddQueryParameter("page", page.ToString());

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<PagedObject<Card>>(response);
        }
    }
}
