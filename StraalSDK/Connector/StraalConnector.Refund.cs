﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Makes full refund (all funds) from a captured transaction.
        /// </summary>
        /// <param name="transactionId">Captured transaction id.</param>
        /// <exception cref="ArgumentNullException">When transaction id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> CreateFullRefund(string transactionId)
        {
            if (string.IsNullOrEmpty(transactionId))
            {
                throw new ArgumentNullException(nameof(transactionId));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/transactions/" + transactionId + "/refund", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(new { });

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }

        /// <summary>
        /// Makes partial refund (specified amount) from a captured transaction.
        /// </summary>
        /// <param name="transactionId">Captured transaction id.</param>
        /// <param name="amount">Amount to refund.</param>
        /// <exception cref="ArgumentNullException">When transaction id is null or empty.</exception>
        /// <exception cref="ArgumentException">When amount is less or equal to zero.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> CreatePartialRefund(string transactionId, int amount)
        {
            if (string.IsNullOrEmpty(transactionId))
            {
                throw new ArgumentNullException(nameof(transactionId));
            }
            if (amount <= 0)
            {
                throw new ArgumentException("Is less or equal to zero", "Amount");
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/transactions/" + transactionId + "/refund", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(new { amount });

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }
    }
}
