﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Makes full capture (all funds) from a pre-authorized transaction.
        /// </summary>
        /// <param name="transactionId">Pre-authorized transaction id.</param>
        /// <exception cref="ArgumentNullException">When transaction id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> CreateFullCapture(string transactionId)
        {
            if (string.IsNullOrEmpty(transactionId))
            {
                throw new ArgumentNullException(nameof(transactionId));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/transactions/" + transactionId + "/capture", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(new { });

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }

        /// <summary>
        /// Makes partial capture (specified amount) from a pre-authorized transaction.
        /// </summary>
        /// <param name="transactionId">Pre-authorized transaction id.</param>
        /// <param name="amount">Amount you want to capture.</param>
        /// <exception cref="ArgumentNullException">When transaction id is null or empty.</exception>
        /// <exception cref="ArgumentException">When amount is less or equal to zero.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> CreatePartialCapture(string transactionId, int amount)
        {
            if (string.IsNullOrEmpty(transactionId))
            {
                throw new ArgumentNullException(nameof(transactionId));
            }
            if(amount <= 0)
            {
                throw new ArgumentException("Is less or equal to zero", nameof(amount));
            }

            var restRequest = new RestRequest(@"/" + _APIVersion + "/transactions/" + transactionId + "/capture", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(new { amount });

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }
    }
}
