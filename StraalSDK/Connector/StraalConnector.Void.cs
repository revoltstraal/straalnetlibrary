﻿using RestSharp;
using StraalSDK.Models;
using System;
using System.Threading.Tasks;

namespace StraalSDK
{
    public partial class StraalConnector
    {
        /// <summary>
        /// Voids a pre-authorized but not yet captured transaction.
        /// </summary>
        /// <param name="transactionId">Pre-authorized transaction id.</param>
        /// <exception cref="ArgumentNullException">When transaction id is null or empty.</exception>
        /// <exception cref="StraalException">When request is not successful.</exception>
        public async Task<Transaction> CreateVoid(string transactionId)
        {
            if (string.IsNullOrEmpty(transactionId))
            {
                throw new ArgumentNullException(nameof(transactionId));
            }

            var restRequest = new RestRequest(@"/"+ _APIVersion +"/transactions/" + transactionId + "/void", Method.POST);
            restRequest.RequestFormat = DataFormat.Json;

            var jsonData = SerializeToJson(new { });

            restRequest.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            var response = await Client.ExecuteTaskAsync(restRequest);

            if (!response.IsSuccessful)
            {
                var errorResponse = ProcessResponse<ErrorResponse>(response);
                throw new StraalException(errorResponse);
            }

            return ProcessResponse<Transaction>(response);
        }
    }
}
