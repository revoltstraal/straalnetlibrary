﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StraalSDK.Constants
{
    public static class TransactionType
    {
        /// <summary>
        /// Disables the card and forbids any future use after the first transaction (e.g. customer did not agree to have his card data saved for future use).
        /// </summary>
        public const string OneShot = "oneshot";

        /// <summary>
        /// Permits future use of the card in subsequent transactions initiated by the customer (e.g. one-click checkout).
        /// </summary>
        public const string OneClick = "oneclick";

        /// <summary>
        /// Permits future use of the card in subsequent transactions initiated by the merchant (subscriptions or other periodic payments).
        /// </summary>
        public const string Recurring = "recurring";
    }
}
