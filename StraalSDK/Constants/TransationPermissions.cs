﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StraalSDK.Constants
{
    public static class TransationPermissions
    {
        /// <summary>
        /// 3D secure
        /// </summary>
        public const string TransationCreateWithCard = "v1.transactions.create_with_card";
        public const string CardsCreate = "v1.cards.create";
    }
}
