﻿using Newtonsoft.Json;
using StraalSDK.Converters;
using StraalSDK.Enums;

namespace StraalSDK.Models
{
    /// <summary>
    /// Card model.
    /// </summary>
    public class Card
    {
        /// <summary>
        /// Unique identificator.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Timestamp of creation.
        /// </summary>
        [JsonProperty("created_at")]
        public int CratedAt { get; set; }

        /// <summary>
        /// Card state: active or disabled.
        /// </summary>
        [JsonProperty("state")]
        [JsonConverter(typeof(CardStateConverter))]
        public CardState State { get; set; }

        /// <summary>
        /// Card brand name.
        /// </summary>
        [JsonProperty("brand")]
        public string Brand { get; set; }

        /// <summary>
        /// Customer name on card.
        /// </summary>
        [JsonProperty("name")]
        public string NameOnCard { get; set; }

        /// <summary>
        /// 6 digits Card BIN number.
        /// </summary>
        [JsonProperty("num_bin")]
        public string CardBIN { get; set; }

        /// <summary>
        /// Last 4 digits of card number.
        /// </summary>
        [JsonProperty("num_last_4")]
        public string LastFourNumbers { get; set; }

        /// <summary>
        /// Card expiration month (format: 1-12).
        /// </summary>
        [JsonProperty("expiry_month")]
        public int ExpiryMonth { get; set; }

        /// <summary>
        /// Card expiration year (in YYYY format).
        /// </summary>
        [JsonProperty("expiry_year")]
        public int ExpiryYear { get; set; }

        /// <summary>
        /// Customer - card owner.
        /// </summary>
        [JsonProperty("customer")]
        public Customer Customer { get; set; }
    }
}
