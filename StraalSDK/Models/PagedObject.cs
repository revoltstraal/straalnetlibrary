﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace StraalSDK.Models
{
    /// <summary>
    /// Return model of all bulk get requests. Pagination is always used.
    /// </summary>
    /// <typeparam name="T">A type of an entity group being retrieved.</typeparam>
    public class PagedObject<T>
    {
        /// <summary>
        /// List of entries returned.
        /// </summary>
        [JsonProperty("data")]
        public List<T> Data { get; set; }

        /// <summary>
        /// Actual page number.
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }

        /// <summary>
        /// Requested number of entries per page.
        /// </summary>
        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        /// <summary>
        /// Total count of entries available.
        /// </summary>
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }
    }
}
