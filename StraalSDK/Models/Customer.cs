﻿using Newtonsoft.Json;

namespace StraalSDK.Models
{
    /// <summary>
    /// Customer model.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Unique identificator.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Timestamp of creation.
        /// </summary>
        [JsonProperty("created_at")]
        public int CratedAt { get; set; }

        /// <summary>
        /// Customer email.
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Optional unique internal code.
        /// </summary>
        [JsonProperty("reference")]
        public string Reference { get; set; }
    }
}
