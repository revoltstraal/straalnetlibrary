﻿using Newtonsoft.Json;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model that is used to request a new CryptKey.
    /// </summary>
    public class CryptKeyRequest
    {
        /// <summary>
        /// Customer id. CryptKey is always created in a customer context.
        /// </summary>
        [JsonProperty("customer_id")]
        public string CustomerId { get; set; }

        /// <summary>
        /// Valid request permission string.
        /// Use "v1.cards.create" to cteate a card.
        /// Use "v1.transactions.create_with_card" to create card with transaction - necessary for 3D secure.
        /// </summary>
        [JsonProperty("permission")]
        public string Permission { get; set; }

        /// <summary>
        /// Set only in case you use permission "v1.transactions.create_with_card". Use it to specify transaction data.
        /// </summary>
        [JsonProperty("transaction")]
        public TransactionCryptKeyRequest Transaction { get; set; }
    }
}