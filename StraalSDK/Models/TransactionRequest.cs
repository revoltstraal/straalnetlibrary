﻿using Newtonsoft.Json;
using StraalSDK.Converters;
using StraalSDK.Enums;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model is used to request a new transaction creation.
    /// </summary>
    public class TransactionRequest
    {
        /// <summary>
        /// Requested transaction amount.
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        /// <summary>
        /// Transaction currency.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Optional internal unique transaction code.
        /// </summary>
        [JsonProperty("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Who initiates a transaction: customer or merchant.
        /// </summary>
        [JsonProperty("initiated_by")]
        [JsonConverter(typeof(InitiatedByConverter))]
        public InitiatedBy InitiatedBy { get; set; }

        /// <summary>
        /// Indicates whether funds should be immediately captured. Set false to create a pre-authorized transaction.
        /// </summary>
        [JsonProperty("capture")]
        public bool Capture { get; set; }
    }
}
