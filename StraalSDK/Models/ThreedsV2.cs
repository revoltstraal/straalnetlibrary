﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StraalSDK.Models
{
    public class ThreedsV2
    {
        [JsonProperty("browser")]
        public Browser Browser { get; set; }
    }
}
