﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StraalSDK.Models
{
    public class Browser
    {
        [JsonProperty("accept_header")]
        public string AcceptHeader { get; set; }

        [JsonProperty("color_depth")]
        public int ColorDepth { get; set; }

        [JsonProperty("java_enabled")]
        public bool JavaEnabled { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("screen_height")]
        public int ScreenHeight { get; set; }

        [JsonProperty("screen_width")]
        public int ScreenWidth { get; set; }

        [JsonProperty("timezone")]
        public int Timezone { get; set; }

        [JsonProperty("user_agent")]
        public string UserAgent { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }
    }
}
