﻿using Newtonsoft.Json;
using StraalSDK.Converters;
using StraalSDK.Enums;
using System.Collections.Generic;

namespace StraalSDK.Models
{
    /// <summary>
    /// Transaction model. Instance of this class is returned when creating or geting a transaction.
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Unique identificator.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Timestamp of creation.
        /// </summary>
        [JsonProperty("created_at")]
        public int CreatedAt { get; set; }

        /// <summary>
        /// Total transaction amount.
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        /// <summary>
        /// Transaction currency.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Payment method: card, SEPA, Pay-by-link.
        /// </summary>
        [JsonProperty("method")]
        [JsonConverter(typeof(PaymentMethodConverter))]
        public PaymentMethod Method { get; set; }

        /// <summary>
        /// Indicates if transaction has been already authorized.
        /// </summary>
        [JsonProperty("authorized")]
        public bool Authorized { get; set; }

        /// <summary>
        /// Indicates if transaction has been already captured.
        /// </summary>
        [JsonProperty("captured")]
        public bool Captured { get; set; }

        /// <summary>
        /// List of all captures performed on this transaction.
        /// </summary>
        [JsonProperty("captures")]
        public List<Capture> Captures { get; set; }

        /// <summary>
        /// Indicates if transaction has been refunded.
        /// </summary>
        [JsonProperty("refunded")]
        public bool Refunded { get; set; }

        /// <summary>
        /// List of all refunds performed on this transaction.
        /// </summary>
        [JsonProperty("refunds")]
        public List<Refund> Refunds { get; set; }

        /// <summary>
        /// Indicates if transaction has been voided.
        /// </summary>
        [JsonProperty("voided")]
        public bool Voided { get; set; }

        /// <summary>
        /// List of all voids performed on this transaction.
        /// </summary>
        [JsonProperty("voids")]
        public List<Void> Voids { get; set; }

        /// <summary>
        /// Optional transaction internal unique code.
        /// </summary>
        [JsonProperty("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Card used to perform this transaction.
        /// </summary>
        [JsonProperty("card")]
        public Card Card { get; set; }

        /// <summary>
        /// Reason of transaction decline.
        /// </summary>
        [JsonProperty("decline_reason")]
        public DeclineReason DeclineReason { get; set; }
    }
}
