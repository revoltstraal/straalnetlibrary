﻿using Newtonsoft.Json;

namespace StraalSDK.Models
{
    /// <summary>
    /// Error model is part of an unsuccessful request.
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Code of an error.
        /// </summary>
        [JsonProperty("code")]
        public int Code { get; set; }

        /// <summary>
        /// Text message that explains an error.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
