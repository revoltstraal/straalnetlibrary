﻿using Newtonsoft.Json;

namespace StraalSDK.Models
{
    /// <summary>
    /// Transaction decline reason. Explains why transaction was rejected.
    /// </summary>
    public class DeclineReason
    {
        /// <summary>
        /// Code of a decline reason.
        /// </summary>
        [JsonProperty("code")]
        public int Code { get; set; }

        /// <summary>
        /// Text description of a decline reason.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
