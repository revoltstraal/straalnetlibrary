﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model of an error response in case of unsuccessful request.
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// List of errors with descriptions.
        /// </summary>
        [JsonProperty("errors")]
        public List<Error> Errors { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
