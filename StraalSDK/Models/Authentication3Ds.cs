﻿using Newtonsoft.Json;
using StraalSDK.Converters;
using StraalSDK.Enums;

namespace StraalSDK.Models
{
    public class Authentication3ds
    {
        /// <summary>
        /// URL to redirect to after successful 3D-Secure authentication (redirects to this URL do not indicate that the transaction succeeded, only that the Strong Customer Authentication (SCA) has been completed successfully)
        /// </summary>
        [JsonProperty("success_url")]
        public string SuccessUrl { get; set; }

        /// <summary>
        /// URL to redirect to after failed 3D-Secure authentication
        /// </summary>
        [JsonProperty("failure_url")]
        public string FailureUrl { get; set; }

        /// <summary>
        /// Optional - for specifing of browser and other other client data
        /// </summary>
        [JsonProperty("threeds_v2")]
        public ThreedsV2 ThreedsV2 { get; set; }
    }
}
