﻿using Newtonsoft.Json;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model of a CryptKey. Should be only used to grant some permission to front-end.
    /// </summary>
    public class CryptKey
    {
        /// <summary>
        /// Unique identificator.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Timestamp of creation.
        /// </summary>
        [JsonProperty("created_at")]
        public int CratedAt { get; set; }

        /// <summary>
        /// Actual key value.
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// Requested permission string.
        /// </summary>
        [JsonProperty("permission")]
        public string Permission { get; set; }

        /// <summary>
        /// Key expiration time (in seconds).
        /// </summary>
        [JsonProperty("ttl")]
        public int TimeToLive { get; set; }
    }
}
