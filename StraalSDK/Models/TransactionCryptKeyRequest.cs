﻿using Newtonsoft.Json;
using StraalSDK.Constants;
using StraalSDK.Converters;
using StraalSDK.Enums;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model of transaction request while creating a CryptKey with permmision "v1.transactions.create_with_card".
    /// </summary>
    public class TransactionCryptKeyRequest
    {
        /// <summary>
        /// Requested transaction amount.
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        /// <summary>
        /// Transaction currency.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Optional internal unique transaction code.
        /// </summary>
        [JsonProperty("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// You can set capture value to false in order to make a preauthorization with 3D Secure.
        /// </summary>
        [JsonProperty("capture")]
        public bool Capture { get; set; } = true;

        /// <summary>
        /// oneshot - disables the card and forbids any future use after the first transaction(e.g.customer did not agree to have his card data saved for future use)
        /// oneclick - permits future use of the card in subsequent transactions initiated by the customer(e.g.one-click checkout)
        /// recurring - permits future use of the card in subsequent transactions initiated by the merchant(subscriptions or other periodic payments)
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; } = TransactionType.OneClick;

        /// <summary>
        /// Authentication_3ds is required if you want to trigger 3DS authentication for customer.
        /// </summary>
        [JsonProperty("authentication_3ds")]
        public Authentication3ds Authentication3ds { get; set; }
    }
}
