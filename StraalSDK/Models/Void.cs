﻿using Newtonsoft.Json;
using StraalSDK.Converters;
using StraalSDK.Enums;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model of transaction void.
    /// </summary>
    public class Void
    {
        /// <summary>
        /// Unique identificator.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Timestamp of creation.
        /// </summary>
        [JsonProperty("created_at")]
        public int CratedAt { get; set; }

        /// <summary>
        /// Void status: succeeded or failed.
        /// </summary>
        [JsonProperty("status")]
        [JsonConverter(typeof(GeneralStatusConverter))]
        public GeneralStatus Status { get; set; }
    }
}
