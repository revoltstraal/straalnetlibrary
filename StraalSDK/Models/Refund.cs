﻿using Newtonsoft.Json;
using StraalSDK.Converters;
using StraalSDK.Enums;

namespace StraalSDK.Models
{
    /// <summary>
    /// Model of a transaction refund.
    /// </summary>
    public class Refund
    {
        /// <summary>
        /// Unique identificator.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Timestamp of creation.
        /// </summary>
        [JsonProperty("created_at")]
        public int CratedAt { get; set; }

        /// <summary>
        /// Refund amount.
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        /// <summary>
        /// Refund status: succeeded, pending or failed.
        /// </summary>
        [JsonProperty("status")]
        [JsonConverter(typeof(GeneralStatusConverter))]
        public GeneralStatus Status { get; set; }
    }
}
